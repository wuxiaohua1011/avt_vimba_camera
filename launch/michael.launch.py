# Copyright 2020-2021, The Autoware Foundation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Launch file for IAC vehicle."""

from fileinput import filename
import os

from ament_index_python import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch.substitutions import TextSubstitution
from launch_ros.actions import Node

namespace = "/camera"


def get_share_file(package_name, file_name):
    return os.path.join(get_package_share_directory(package_name), file_name)


def generate_launch_description():

    # Print to terminal to notify cameras are enabled
    """Launch all packages for the vehicle in IAC."""
    
    front_left_center_cam = Node(
                package="avt_vimba_camera",
                namespace=namespace,
                name="front_left_center_cam",
                executable="mono_camera_node",
                output="screen",
                parameters=[{
                    "default_camera_param_file": get_share_file(package_name="avt_vimba_camera", 
                                                                file_name="config/front_left_center_cam.yaml"),
                    "camera_ip_addr": "10.42.21.51",
                    "frame_id":"front_left_center_cam"
                }
                ],
                remappings=[],
            )
    front_left_fisheye_cam = Node(
                package="avt_vimba_camera",
                namespace=namespace,
                name="front_left_fisheye_cam",
                executable="mono_camera_node",
                output="screen",
                parameters=[{
                    "default_camera_param_file":  get_share_file(package_name="avt_vimba_camera", file_name="config/front_left_fisheye_cam.yaml"),
                    "camera_ip_addr": "10.42.21.61",
                    "frame_id":"front_left_fisheye_cam"
                }
                ],
                remappings=[],
            )
    front_right_center_cam = Node(
                package="avt_vimba_camera",
                namespace=namespace,
                name="front_right_center_cam",
                executable="mono_camera_node",
                output="screen",
                parameters=[{
                    "default_camera_param_file": get_share_file(package_name="avt_vimba_camera", file_name="config/front_right_center_cam.yaml"
),
                    "camera_ip_addr": "10.42.21.52",
                    "frame_id":"front_right_center_cam"
                }
                ],
                remappings=[],
            )
    front_right_fisheye_cam = Node(
                package="avt_vimba_camera",
                namespace=namespace,
                name="front_right_fisheye_cam",
                executable="mono_camera_node",
                output="screen",
                parameters=[{
                    "default_camera_param_file": get_share_file(package_name="avt_vimba_camera", file_name="config/front_right_fisheye_cam.yaml"),
                    "camera_ip_addr": "10.42.21.62",
                    "frame_id":"front_right_fisheye_cam"
                }
                ],
                remappings=[],
            )
    
    rviz_node = Node(
        package="rviz2",
        executable="rviz2",
        name="rviz2",
        output="screen",
        arguments=["-d", get_share_file(package_name="avt_vimba_camera", file_name="config/rviz.rviz")]
    )
    return LaunchDescription(
        [
            front_left_center_cam,
            # front_left_fisheye_cam,
            # front_right_center_cam,
            # front_right_fisheye_cam,
            # rviz_node
        ]
    )
